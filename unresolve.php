<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);
require('lib/init.php');

if (is_array($_POST['hashes'])) {
    $hashes = $_POST['hashes'];
} else {
    $hashes = explode(',', $_POST['hashes']);
}

$database = new Database();

foreach ($hashes as $hash) {
    $database->log_hash_unresolved($hash);
}

header('Location: index.php');
