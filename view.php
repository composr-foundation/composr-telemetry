<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);
require('lib/init.php');

$hash = $_GET['hash'];
$start = @intval($_GET['start']);
$max = 30;

$_hash = htmlentities($hash);

echo <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Composr CMS telemetry &gt; {$_hash}</title>
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
    <h1><a href="index.php">Composr CMS telemetry</a> &gt; {$_hash}</h1>

END;

$database = new Database();

$hash_row = $database->load_hash($hash);

$_resolved_at = ($hash_row['resolved_at'] === null) ? '<em>N/A</em>' : htmlentities(strftime('%d/%m/%Y %H:%I:%S', $hash_row['resolved_at']));
if ($hash_row['mantis_id'] !== null) {
    $_mantis_id = '<a href="https://compo.sr/tracker/view.php?id=' . strval($hash_row['mantis_id']) . '">' . strval($hash_row['mantis_id']) . '</a>';
} else {
    $_mantis_id = '';
}
$_notes = htmlentities($hash_row['notes']);

echo <<<END
    <table>
        <colspan>
            <col style="width: 150px" />
        </colspan>

        <tbody>
            <tr>
                <th>Resolved at</th>
                <td>{$_resolved_at}</td>
            </tr>

            <tr>
                <th>Mantis ID</th>
                <td>{$_mantis_id}</td>
            </tr>

            <tr>
                <th>Notes</th>
                <td>{$_notes}</td>
            </tr>
        </tbody>
    </table>

    <h2>Incidents logged</h2>
END;

$emails = $database->enumerate_hash_emails($hash, $start, $max);

foreach ($emails as $email) {
    $_email_id = htmlentities($email['email_id']);
    $_date = htmlentities(strftime('%d/%m/%Y %H:%I:%S', $email['timestamp']));
    $_from_email = htmlentities($email['from_email']);
    $_site_name = htmlentities($email['site_name']);
    $_version = htmlentities($email['version']);
    $_php_version = htmlentities($email['php_version']);
    $_page = htmlentities($email['page']);
    $_url = htmlentities($email['url']);
    $_message = htmlentities($email['message']);

echo <<<END
    <table>
        <colspan>
            <col style="width: 150px" />
        </colspan>

        <tbody>
            <tr>
                <th>E-mail ID</th>
                <td>{$_email_id}</td>
            </tr>
            <tr>
                <th>Date</th>
                <td>{$_date}</td>
            </tr>
            <tr>
                <th>From e-mail</th>
                <td>{$_from_email}</td>
            </tr>
            <tr>
                <th>Site name</th>
                <td>{$_site_name}</td>
            </tr>
            <tr>
                <th>Version</th>
                <td>{$_version}</td>
            </tr>
            <tr>
                <th>PHP version</th>
                <td>{$_php_version}</td>
            </tr>
            <tr>
                <th>Page</th>
                <td>{$_page}</td>
            </tr>
            <tr>
                <th>URL</th>
                <td>{$_url}</td>
            </tr>
            <tr>
                <th>Message</th>
                <td>{$_message}</td>
            </tr>
            <tr>
                <th>Trace</th>
                <td>
END;
    foreach (json_decode($email['trace']) as $frame) {
        $_file = ($frame->file);
        $_line = htmlentities($frame->line);
        $_function = ($frame->function);
        $_args = ($frame->args);

echo <<<END
                    <table>
                        <colspan>
                            <col style="width: 100px" />
                        </colspan>
                        <tbody>
                            <tr>
                                <th>File</th>
                                <td>{$_file}</td>
                            </tr>
                            <tr>
                                <th>Line</th>
                                <td>{$_line}</td>
                            </tr>
                            <tr>
                                <th>Function</th>
                                <td>{$_function}</td>
                            </tr>
                            <tr>
                                <th>Args</th>
                                <td>{$_args}</td>
                            </tr>
                        </tbody>
                    </table>
                    <br />
END;
    }
echo <<<END
                </td>
            </tr>
        </tbody>
    </table>
    <br />
END;
}

$_hash = htmlentities($hash);

if (count($emails) == $max) {
    $next = $start + $max;
echo <<<END
    <p>
        &raquo; <a href="view.php?hash={$_hash}&amp;start={$next}">Next</a>
    </p>
END;
}

echo <<<END
<h2>Actions</h2>

<form action="resolve.php" method="post">
    <fieldset>
        <legend>Make issue as resolved</legend>

        <input type="hidden" name="hashes" value="{$_hash}" />

        <p>
            <label for="mantis_id">Mantis ID:</label>
            <input name="mantis_id" id="mantis_id" type="number" value="" />
        </p>

        <p>
            <label for="notes">Notes:</label><br />
            <textarea cols="100" rows="5" name="notes" id="notes"></textarea>
        </p>

        <p>
            <input type="submit" value="Resolve" />
        </p>
    </fieldset>
</form>
END;

echo <<<END
</body>
</html>
END;
