<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

class Database
{
    protected $connection;

    public function __construct()
    {
        $this->connection = new mysqli(DATABASE_HOST, DATABASE_USER, DATABASE_PASS, DATABASE_NAME);
    }

    public function __destruct()
    {
        if ($this->connection) {
            $this->connection->close();
        }
    }

    public function log_email($struct, $subject, $body)
    {
        $hash = $this->generate_email_hash($struct);

        $sql = 'SELECT * FROM hashes WHERE hash=\'' . $this->connection->escape_string($hash) . '\'';
        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
        if (!$result->fetch_assoc()) {
            $sql = 'INSERT INTO hashes (hash,resolved_at,mantis_id,notes) VALUES (\'' . $this->connection->escape_string($hash) . '\',null,null,\'\')';
            $result2 = $this->connection->query($sql);
            if (!$result2) {
                trigger_error($this->connection->error . ' [' . $sql . ']');
            }
        }
        $result->free();

        $sql = 'REPLACE INTO hashed_emails
        (email_id,hash,timestamp,from_email,site_name,version,php_version,page,url,message,trace,subject,body)
        VALUES (
            \'' . $this->connection->escape_string($struct['email_id']) . '\',
            \'' . $this->connection->escape_string($hash) . '\',
            ' . strval($struct['timestamp']) . ',
            \'' . $this->connection->escape_string($struct['from_email']) . '\',
            \'' . $this->connection->escape_string($struct['site_name']) . '\',
            \'' . $this->connection->escape_string($struct['version']) . '\',
            \'' . $this->connection->escape_string($struct['php_version']) . '\',
            \'' . $this->connection->escape_string($struct['page']) . '\',
            \'' . $this->connection->escape_string($struct['url']) . '\',
            \'' . $this->connection->escape_string($struct['message']) . '\',
            \'' . $this->connection->escape_string(json_encode($struct['trace'])) . '\',
            \'' . $this->connection->escape_string($subject) . '\',
            \'' . $this->connection->escape_string($body) . '\'
        )';
        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
    }

    protected function generate_email_hash($struct)
    {
        $hash_arr = [];

        $message_stripped = $struct['message'];
        $message_stripped = preg_replace('#"[^"]*"#', '', $message_stripped);
        $message_stripped = preg_replace("#'[^']*'#", '', $message_stripped);
        $message_stripped = preg_replace('#\d+#', '', $message_stripped);
        $message_stripped = preg_replace('#/\*.*\*/#U', '', $message_stripped);

        $hash_arr[] = $message_stripped;
        foreach ($struct['trace'] as $i => $frame) {
            if ($i == 8) { // Don't go deeper
                break;
            }

            $hash_arr[] = $frame['file'];
            $hash_arr[] = $frame['line'];
            $hash_arr[] = $frame['function'];
        }

        return md5(json_encode($hash_arr));
    }

    public function enumerate_hashes($start = 0, $max = 30, $show_unresolved = true, $show_resolved = false)
    {
        $sql = 'SELECT *,(SELECT COUNT(*) FROM hashed_emails eee WHERE eee.hash=h.hash) AS total FROM hashes h JOIN hashed_emails e ON h.hash=e.hash AND e.email_id=(SELECT email_id FROM hashed_emails ee WHERE ee.hash=h.hash ORDER BY timestamp DESC LIMIT 1)';
        if (!$show_unresolved || !$show_resolved) {
            if ($show_unresolved) {
                $sql .= ' WHERE resolved_at IS NULL';
            } elseif ($show_resolved) {
                $sql .= ' WHERE resolved_at IS NOT NULL';
            } else {
                $sql .= ' WHERE 1=0';
            }
        }
        $sql .= ' ORDER BY total DESC';
        $sql .= ' LIMIT ' . strval($start) . ',' . strval($max);

        $rows = [];

        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        $result->free();

        return $rows;
    }

    public function load_hash($hash)
    {
        $sql = 'SELECT *,(SELECT COUNT(*) FROM hashed_emails eee WHERE eee.hash=h.hash) AS total FROM hashes h';
        $sql .= ' WHERE hash=\'' . $this->connection->escape_string($hash) . '\'';

        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
        $row = $result->fetch_assoc();
        $result->free();

        return $row;
    }

    public function enumerate_hash_emails($hash, $start = 0, $max = 30)
    {
        $sql = 'SELECT * FROM hashed_emails WHERE hash=\'' . $this->connection->escape_string($hash) . '\'';
        $sql .= ' ORDER BY timestamp DESC';
        $sql .= ' LIMIT ' . strval($start) . ',' . strval($max);

        $rows = [];

        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
        while ($row = $result->fetch_assoc()) {
            $rows[] = $row;
        }
        $result->free();

        return $rows;
    }

    public function log_hash_resolved($hash, $mantis_id = null, $notes = '')
    {
        $sql = 'UPDATE hashes SET resolved_at=' . strval(time());
        if ($mantis_id === null) {
            $sql .= ',mantis_id=NULL';
        } else {
            $sql .= ',mantis_id=' . strval($mantis_id);
        }
        $sql .= ',notes=\'' . $this->connection->escape_string($notes) . '\'';
        $sql .= ' WHERE hash=\'' . $this->connection->escape_string($hash) . '\'';

        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
    }

    public function log_hash_unresolved($hash)
    {
        $sql = 'UPDATE hashes SET resolved_at=NULL,mantis_id=NULL,notes=\'\'';
        $sql .= ' WHERE hash=\'' . $this->connection->escape_string($hash) . '\'';

        $result = $this->connection->query($sql);
        if (!$result) {
            trigger_error($this->connection->error . ' [' . $sql . ']');
        }
    }
}
