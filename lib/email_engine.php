<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

class EmailEngine
{
    protected $imap;

    public function __construct($host, $port, $protocol, $user, $pass)
    {
        $this->imap = imap_open('{' . $host . ':' . strval($port) . '/' . $protocol . '}INBOX', $user, $pass);
    }

    public function __destruct()
    {
        if ($this->imap) {
            imap_close($this->imap);
        }
    }

    public function list_emails()
    {
        $_emails = imap_search($this->imap, 'ALL');

        $emails = [];

        if ($_emails === false) { // imap_search returns false if no results
            return $emails;
        }

        foreach ($_emails as $mid) {
            $h = imap_headerinfo($this->imap, $mid);
            $s = imap_fetchstructure($this->imap, $mid);

            $email_id = $h->message_id;

            $emails[$email_id] = [$h, $s, $mid];
        }

        return $emails;
    }

    public function parse_email($email_id, $h, $s, $mid)
    {
        $subject = $h->subject;

        $matches = [];
        if (preg_match('#^(.*): (.*): An error occurred on the \'(.*)\' page$#', $subject, $matches) == 0) {
            return null;
        }
        $site_name = $matches[1];
        $version = $matches[2];
        $page = $matches[3];

        $this->getmsg($this->imap, $mid, $h, $s);
        $body = $this->htmlmsg;

        if (preg_match('#An error occurred at.*href="([^"]*)"#Us', $body, $matches) != 0) {
            $url = html_entity_decode($matches[1], ENT_QUOTES);
        } else {
            $url = '';
        }

        if (preg_match('#The full error details follow:(\s*?(<br\s*/?>)*?\s*?)(.*)(\s*?(<br\s*/?>)*?\s*?)\s+\(version:\s+([^,]*),\s+PHP\s+version:\s+([^,]*), #Us', $body, $matches) != 0) {
            $message = html_entity_decode($matches[3], ENT_QUOTES);
            $version = html_entity_decode($matches[6], ENT_QUOTES);
            $php_version = html_entity_decode($matches[7], ENT_QUOTES);
        } else {
            $message = '';
            $version = '';
            $php_version = '';
        }

        $num_matches = preg_match_all('#<tr>\s*<th>\s*File\s*</th>\s*<td>\s*(.*)\s*</td>\s*</tr>\s*<tr>\s*<th>\s*Line\s*</th>\s*<td>\s*(.*)\s*</td>\s*</tr>\s*<tr>\s*<th>\s*Function\s*</th>\s*<td>\s*(.*)\s*</td>\s*</tr>.*<tr>\s*<th>\s*Args\s*</th>\s*<td>\s*(.*)\s*</td>\s*</tr>\s*#Us', $body, $matches);
        $trace = [];
        for ($i = 0; $i < $num_matches; $i++) {
            $trace[] = [
                'file' => $matches[1][$i],
                'line' => $matches[2][$i],
                'function' => $matches[3][$i],
                'args' => $this->ascii_only($matches[4][$i]),
            ];
        }

        $struct = [
            'email_id' => $email_id,
            'timestamp' => $h->udate,
            'from_email' => substr($h->reply_toaddress, 0, 255),

            'site_name' => $this->ascii_only(substr($site_name, 0, 255)),
            'version' => substr($version, 0, 20),
            'php_version' => substr($php_version, 0, 80),
            'page' => $this->ascii_only(substr($page, 0, 255)),
            'url' => $this->ascii_only($url),
            'message' => $this->ascii_only($message),
            'trace' => $trace,
        ];

        return [$struct, $this->ascii_only($subject), $this->ascii_only($body)];
    }

    protected function ascii_only($in)
    {
        return preg_replace('/[^\x20-\x7E]/', '', $in);
    }

    public function delete_email($mid)
    {
        imap_delete($this->imap, $mid);
    }

    // Below is based on a comment at https://www.php.net/manual/en/function.imap-fetchstructure.php...

    protected $charset;
    protected $htmlmsg;
    protected $plainmsg;

    protected function getmsg($mbox, $mid, $h, $s)
    {
        $this->charset = '';
        $this->htmlmsg = '';
        $this->plainmsg = '';

        if (empty($s->parts)) {  // simple
            $this->getpart($mbox, $mid, $s, 0);
        }  // pass 0 as part-number
        else {  // multipart: cycle through each part
            foreach ($s->parts as $partno0 => $p) {
                $this->getpart($mbox, $mid, $p, $partno0 + 1);
            }
        }
    }

    protected function getpart($mbox, $mid, $p, $partno)
    {
        // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple

        // DECODE DATA
        $data = ($partno) ?
            imap_fetchbody($mbox, $mid, $partno) :  // multipart
            imap_body($mbox, $mid);  // simple
        // Any part may be encoded, even plain text messages, so check everything.
        if ($p->encoding == 4) {
            $data = quoted_printable_decode($data);
        } elseif ($p->encoding == 3) {
            $data = base64_decode($data);
        }

        // PARAMETERS
        // get all parameters, like charset, etc.
        $params = [];
        if (!empty($p->parameters)) {
            foreach ($p->parameters as $x) {
                $params[strtolower($x->attribute)] = $x->value;
            }
        }
        if (!empty($p->dparameters)) {
            foreach ($p->dparameters as $x) {
                $params[strtolower($x->attribute)] = $x->value;
            }
        }

        // TEXT
        if ($p->type == 0 && !empty($data)) {
            // Messages may be split in different parts because of inline attachments,
            // so append parts together with blank row.
            if (strtolower($p->subtype) == 'plain') {
                $this->plainmsg .= trim($data) . "\n\n";
            } else {
                $this->htmlmsg .= $data . "<br><br>";
            }
            if (!empty($params['charset'])) {
                $this->charset = $params['charset'];  // assume all parts are same charset
            }
        }

        // EMBEDDED MESSAGE
        // Many bounce notifications embed the original message as type 2,
        // but AOL uses type 1 (multipart), which is not handled here.
        // There are no PHP functions to parse embedded messages,
        // so this just appends the raw source to the main message.
        elseif ($p->type == 2 && !empty($data)) {
            $this->plainmsg .= $data . "\n\n";
        }

        // SUBPART RECURSION
        if (!empty($p->parts)) {
            foreach ($p->parts as $partno0 => $p2) {
                $this->getpart($mbox, $mid, $p2, $partno . '.' . ($partno0 + 1));
            }  // 1.2, 1.2.1, etc.
        }
    }
}
