<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

error_reporting(E_ALL | E_STRICT);

ini_set('display_errors', '1');
ini_set('error_log', 'errorlog.txt');

set_error_handler(function($errno, $errstr, $errfile, $errline) {
    if (!(error_reporting() & $errno)) {
        return;
    }

    $msg = "Error [$errno] $errstr on line $errline in file $errfile";
    echo $msg;
    error_log($msg);

    exit(1);
});

require('_config.php');

if (!class_exists('mysqli')) {
    trigger_error('mysqli required');
}

if ((!function_exists('imap_open')) && (php_sapi_name() == 'cli')) {
    trigger_error('imap required');
}

require('lib/database.php');
require('lib/email_engine.php');
