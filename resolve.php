<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);
require('lib/init.php');

if (empty($_POST['hashes'])) {
	exit('Nothing was selected');
}
if (is_array($_POST['hashes'])) {
    $hashes = $_POST['hashes'];
} else {
    $hashes = explode(',', $_POST['hashes']);
}

$mantis_id = ($_POST['mantis_id'] == '') ? null : intval($_POST['mantis_id']);
$notes = $_POST['notes'];

$database = new Database();

foreach ($hashes as $hash) {
    $database->log_hash_resolved($hash, $mantis_id, $notes);
}

header('Location: index.php');
