<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);
require('lib/init.php');

$database = new Database();

$i = 0;
while (defined('EMAIL_' . strval($i) . '_HOST')) {
    $email_engine = new EmailEngine(constant('EMAIL_' . strval($i) . '_HOST'), constant('EMAIL_' . strval($i) . '_PORT'), constant('EMAIL_' . strval($i) . '_PROTOCOL'), constant('EMAIL_' . strval($i) . '_USER'), constant('EMAIL_' . strval($i) . '_PASS'));

    $emails = $email_engine->list_emails();
    foreach ($emails as $email_id => $email) {
        list($h, $s, $mid) = $email;

        list($struct, $subject, $body) = $email_engine->parse_email($email_id, $h, $s, $mid);

        if ($struct !== null) {
            $database->log_email($struct, $subject, $body);

            $email_engine->delete_email($mid);

            echo "Processed e-mail\n";
        }
    }

    echo "Finished " . constant('EMAIL_' . strval($i) . '_USER') . "\n";

    $i++;
}

echo "Done\n";
