<?php /*
 Composr Telemetry

 http://opensource.org/licenses/cpal_1.0 Common Public Attribution License
*/

chdir(__DIR__);
require('lib/init.php');

$start = @intval($_GET['start']);
$max = isset($_GET['max']) ? intval($_GET['max']) : 60;
$show_unresolved = !isset($_GET['unresolved']) || !empty($_GET['unresolved']);
$show_resolved = !empty($_GET['resolved']);

$_form_url = $show_unresolved ? 'resolve.php' : 'unresolve.php';
$_label = $show_unresolved ? 'resolved (with no notes or mantis ID)' : 'unresolved';

echo <<<END
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Composr CMS telemetry</title>
    <link href="styles.css" rel="stylesheet" />
</head>
<body>
    <h1>Composr CMS telemetry</h1>

    <form action="{$_form_url}" method="post">
        <input type="hidden" name="mantis_id" value="" />
        <input type="hidden" name="notes" value="" />

        <table>
            <thead>
                <tr>
                    <th>Hash</th>
                    <th>Sample message</th>
END;
if ($show_resolved) {
echo <<<END
                    <th>Mantis ID</th>
                    <th>Notes</th>
END;
}
echo <<<END
                    <th>Total</th>
                    <th>Mark</th>
                </tr>
            </thead>

            <tbody>
END;

$database = new Database();
$hashes = $database->enumerate_hashes($start, $max, $show_unresolved, $show_resolved);

foreach ($hashes as $hash) {
    $_hash = htmlentities($hash['hash']);
    $_mantis_id = htmlentities($hash['mantis_id']);
    $_notes = htmlentities($hash['notes']);
    $_message = htmlentities($hash['message']);
    $_total = htmlentities(number_format($hash['total']));

echo <<<END
                <tr>
                    <td><a href="view.php?hash={$_hash}">{$_hash}</a></td>
                    <td>{$_message}</td>
END;
if ($show_resolved) {
echo <<<END
                    <td>{$_mantis_id}</td>
                    <td>{$_notes}</td>
END;
}
echo <<<END
                    <td>{$_total}</td>
                    <td><input type="checkbox" name="hashes[]" value="{$_hash}" /></td>
                </tr>
END;
}

if (empty($hashes)) {
echo <<<END
                <tr>
                    <td colspan="6"><em>No results</em></td>
                </tr>
END;
}

echo <<<END
            </tbody>
END;

if (!empty($hashes)) {
echo <<<END
            <tfoot>
                <tr>
                    <td colspan="6" style="text-align: right">
                        <input type="button" value="Toggle all" onclick="toggle_all(this.form);" />
                    </td>
                </tr>
            </tfoot>
END;
}

echo <<<END
        </table>
END;

if (!empty($hashes)) {
echo <<<END
        <p style="text-align: right">
            <input type="submit" value="Quickly mark all ticked {$_label}" />
        </p>
END;
}

echo <<<END
    </form>

    <script>
        function toggle_all(form)
        {
            for (var i = 0; i < form.elements.length; i++) {
                if (form.elements[i].type == 'checkbox') {
                    form.elements[i].checked = !form.elements[i].checked;
                }
            }
        }
    </script>

    <hr />
END;

if (count($hashes) == $max) {
    $next = $start + $max;
    $_resolved = $show_resolved ? '1': ' 0';
    $_unresolved = $show_unresolved ? '1': ' 0';
echo <<<END
    <p>
        &raquo; <a href="index.php?start={$next}&amp;resolved={$_resolved}&amp;unresolved={$_unresolved}">Next</a>
    </p>
END;
}

if ($show_resolved) {
echo <<<END
    <p>
        &raquo; <a href="index.php?start={$start}&amp;resolved=0&amp;unresolved=1">Show unresolved instead</a>
    </p>
END;
} else {
echo <<<END
    <p>
        &raquo; <a href="index.php?start={$start}&amp;resolved=1&amp;unresolved=0">Show resolved instead</a>
    </p>
END;
}

echo <<<END
</body>
</html>
END;
