DROP TABLE IF EXISTS `hashes`;

CREATE TABLE `hashes` (
  `hash` varchar(32) NOT NULL PRIMARY KEY,
  `resolved_at` INT NULL,
  `mantis_id` int NULL,
  `notes` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE INDEX hashes_resolved_at_IDX USING BTREE ON hashes (resolved_at);

DROP TABLE IF EXISTS `hashed_emails`;

CREATE TABLE `hashed_emails` (
  `email_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL PRIMARY KEY,
  `hash` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` int NOT NULL,
  `from_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `site_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `version` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `php_version` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `page` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `trace` longtext COLLATE utf8_unicode_ci NOT NULL,
  `subject` text COLLATE utf8_unicode_ci NOT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE INDEX hashed_emails_hash_IDX USING BTREE ON hashed_emails (hash);
